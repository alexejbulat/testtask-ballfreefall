﻿using UnityEngine;
using System.Collections;

public enum BallBehaviorType
{
    Simple,
    ControllablePhysic,
    Math
}

public static class BallsConstructor
{
    private readonly static float LimitOffsetX = 4f;
    private readonly static float LimitOffsetMinY = 2f;
    private readonly static float LimitOffsetMaxY = 3.5f;

    public static BasicBallBehavior CreateBall(BallBehaviorType type, Collider plato)
    {
        var createdBall = CreateBallObject(plato, type);
        var ball = createdBall.AddComponent(ConvertTypeToSystemType(type)) as BasicBallBehavior;
        return ball;
    }

    private static GameObject CreateBallObject(Collider plato, BallBehaviorType type)
    {
        var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        var rootBounds = plato.bounds;
        go.transform.position = rootBounds.center + GetOffsetDirectionByType(type) * GetOffsetX() + Vector3.up * GetOffsetY();
        go.transform.rotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;

        go.renderer.material.color = GetColorByType(type);
        return go;
    }

    private static float GetOffsetX()
    {
        return LimitOffsetX;
    }

    private static float GetOffsetY()
    {
        return Mathf.Clamp(Random.value * LimitOffsetMaxY, LimitOffsetMinY, LimitOffsetMaxY);
    }

    private static Color GetColorByType(BallBehaviorType type)
    {
        Color resultColor = Color.magenta;
        switch (type)
        {
            case BallBehaviorType.Simple:
                resultColor = Color.red;
                break;
            case BallBehaviorType.Math:
                resultColor = Color.blue;
                break;
            case BallBehaviorType.ControllablePhysic:
                resultColor = Color.green;
                break;
        }   
        return resultColor;
    }

    private static Vector3 GetOffsetDirectionByType(BallBehaviorType type)
    {
        Vector3 offsetDirection = Vector3.zero;
        switch (type)
        {
            case BallBehaviorType.Simple:
                offsetDirection = Vector3.zero;
                break;
            case BallBehaviorType.Math:
                offsetDirection = Vector3.right;
                break;
            case BallBehaviorType.ControllablePhysic:
                offsetDirection = Vector3.left;
                break;
        }
        return offsetDirection;
    }

    private static System.Type ConvertTypeToSystemType(BallBehaviorType type)
    {
        System.Type ballType = typeof(FirstBallBehavior);
        switch (type)
        {
            case BallBehaviorType.Simple:
                ballType = typeof(FirstBallBehavior);
                break;
            case BallBehaviorType.Math:
                ballType = typeof(ThirdBallBehavior);
                break;
            case BallBehaviorType.ControllablePhysic:
                ballType = typeof(SecondBallBehavior);
                break;            
        }
        return ballType;
    }
}