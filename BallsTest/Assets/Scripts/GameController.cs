﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public Collider Plato;

    private void Start()
    {
        InstantiateBalls();
    }

    private void InstantiateBalls()
    {
        BallsConstructor.CreateBall(BallBehaviorType.Math, Plato);
        BallsConstructor.CreateBall(BallBehaviorType.Simple, Plato);
        BallsConstructor.CreateBall(BallBehaviorType.ControllablePhysic, Plato);
    }
}
