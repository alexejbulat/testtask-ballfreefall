﻿using UnityEngine;
using System.Collections;

public class BasicBallBehavior : MonoBehaviour
{
    protected Vector3 _initialPosition;
    protected Color _initialColor;

    protected virtual void Awake()
    {
        gameObject.AddComponent<Rigidbody>();
    }

    protected virtual void Start()
    {
        _initialPosition = transform.position;
        _initialColor = renderer.material.color;
        rigidbody.mass = Random.Range(10f, 100f);
    }

    protected virtual void Update()
    {
        renderer.material.color = Color.Lerp(renderer.material.color, _initialColor, Time.deltaTime);
    }

    protected void OnCollisionEnter(Collision collision)
    {
        OnGroundHit();
    }

    protected void OnTriggerEnter(Collider collider)
    {
        OnGroundHit();
    }

    protected virtual void OnGroundHit()
    {
        renderer.material.color = Color.cyan;
    }
}
