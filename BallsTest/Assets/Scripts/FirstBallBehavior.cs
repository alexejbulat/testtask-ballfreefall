﻿using UnityEngine;
using System.Collections;

public class FirstBallBehavior : BasicBallBehavior
{
    protected override void Start()
    {
        base.Start();
        collider.material.bounciness = 1f;
        collider.material.bounceCombine = PhysicMaterialCombine.Maximum;
    }
}
