﻿using UnityEngine;
using System.Collections;

public class SecondBallBehavior : BasicBallBehavior
{
    protected override void OnGroundHit()
    {
        base.OnGroundHit();
        rigidbody.velocity = Vector3.up * Mathf.Sqrt(2f * Physics.gravity.magnitude * _initialPosition.y);
    }
}
