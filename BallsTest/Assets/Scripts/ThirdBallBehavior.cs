﻿using UnityEngine;
using System.Collections;

public class ThirdBallBehavior : BasicBallBehavior
{
    private Vector3 velocity = Vector3.zero;

    protected override void Start()
    {
        base.Start();
        collider.isTrigger = true;
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;
    }

    protected override void Update()
    {
        base.Update();
        velocity = Physics.gravity * Time.deltaTime + velocity;
        transform.position += velocity * Time.deltaTime;
    }

    protected override void OnGroundHit()
    {
        base.OnGroundHit();
        float COR = 1f;
        velocity = Vector3.up * COR * Mathf.Sqrt(2f * Physics.gravity.magnitude * _initialPosition.y);
    }
}
