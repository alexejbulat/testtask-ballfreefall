## Test Task (Balls free fall) ##

**Target:**
Create a scene in Unity, that contains a flat surface, and 3 spheres that are placed above it.
Each sphere must start at some height above the surface, and fall towards it.
On impact with the surface, each sphere must bounce straight up until it reaches the exact height from which it fell, as the process repeats infinitely.
The sphere's movement should be realistic.
Each of the spheres must bounce using a unique movement mechanism that is different from the others.

### Requirements ###

* Unity3d 4.6.1f1

### Description ###

There is 3 types of balls with different movement behaviors.
* FirstBallBehavior.cs (Red Ball): shows basic Unity3d bounce mechanic with PhysicsMaterials. This is example only for highlighting easiest way to make object bounce, and some Unity physic engine mistakes. 
* SecondBallBehavior.cs (Green Ball): ball uses original Unity3d physic engine. 
* ThirdBallBehavior.cs (Blue Ball): ball uses full mathematical solution for Free fall modeling. 


*Made by Alexej Bulat*